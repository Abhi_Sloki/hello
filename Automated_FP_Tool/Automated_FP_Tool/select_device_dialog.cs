﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Automated_FP_Tool
{
    public partial class select_device_dialog : Form
    {
        List<string> dev_list = new List<string>();
        string selected_string;
        UInt16 dev_count;
        public select_device_dialog()
        {
            InitializeComponent();
            dev_count = 0;
        }

        public void set_serial_num(List<string> ser_num_list)
        {
            dev_list = ser_num_list;
            dev_count = (UInt16) ser_num_list.Count();
            if(dev_count == 1)
            {
                sel_dev_rb1.Text = ser_num_list[0];
                rb_sel_dev.Visible = false;
                rb3_sel_dev.Visible = false;
                rb4_sel_dev.Visible = false;
            }
            else
                if(dev_count == 2)
                {
                    sel_dev_rb1.Text = ser_num_list[0];
                    rb_sel_dev.Text = ser_num_list[1];
                    rb3_sel_dev.Visible = false;
                    rb4_sel_dev.Visible = false;
                }
                else
                    if(dev_count == 3)
                    {
                        sel_dev_rb1.Text = ser_num_list[0];
                        rb_sel_dev.Text = ser_num_list[1];
                        rb3_sel_dev.Text = ser_num_list[2];
                        
                        rb4_sel_dev.Visible = false;

                    }
                    else
                        if(dev_count == 4)
                        {
                            sel_dev_rb1.Text = ser_num_list[0];
                            rb_sel_dev.Text = ser_num_list[1];
                            rb3_sel_dev.Text = ser_num_list[2];
                            rb4_sel_dev.Text = ser_num_list[3];
                        }
            
        }

        public string get_selected_ser_num ()
        {
            return selected_string;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if(dev_count > 3)
            {
                selected_string = dev_list[2];
            }
            
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if(dev_count > 2)
            {
                selected_string = dev_list[3];
            }
            
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if(dev_count > 1)
            {
                selected_string = dev_list[1];
            }
            
        }

        private void sel_dev_rb1_CheckedChanged(object sender, EventArgs e)
        {
            if (dev_count != 0) 
            {
                selected_string = dev_list[0];
            }
            
        }

        private void sel_dev_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sel_dev_cancel_Click(object sender, EventArgs e)
        {
            selected_string = null;
            this.Close();
        }

        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // select_device_dialog
        //    // 
        //    this.ClientSize = new System.Drawing.Size(284, 261);
        //    this.Name = "select_device_dialog";
        //    this.Load += new System.EventHandler(this.select_device_dialog_Load);
        //    this.ResumeLayout(false);

        //}

       
    }
}
