﻿namespace Automated_FP_Tool
{
    partial class select_device_dialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sel_dev_rb1 = new System.Windows.Forms.RadioButton();
            this.sel_dev_ok = new System.Windows.Forms.Button();
            this.sel_dev_cancel = new System.Windows.Forms.Button();
            this.rb_sel_dev = new System.Windows.Forms.RadioButton();
            this.rb3_sel_dev = new System.Windows.Forms.RadioButton();
            this.rb4_sel_dev = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // sel_dev_rb1
            // 
            this.sel_dev_rb1.AutoSize = true;
            this.sel_dev_rb1.Location = new System.Drawing.Point(12, 12);
            this.sel_dev_rb1.Name = "sel_dev_rb1";
            this.sel_dev_rb1.Size = new System.Drawing.Size(82, 17);
            this.sel_dev_rb1.TabIndex = 0;
            this.sel_dev_rb1.TabStop = true;
            this.sel_dev_rb1.Text = "SBUSCAN1";
            this.sel_dev_rb1.UseVisualStyleBackColor = true;
            this.sel_dev_rb1.CheckedChanged += new System.EventHandler(this.sel_dev_rb1_CheckedChanged);
            // 
            // sel_dev_ok
            // 
            this.sel_dev_ok.Location = new System.Drawing.Point(39, 176);
            this.sel_dev_ok.Name = "sel_dev_ok";
            this.sel_dev_ok.Size = new System.Drawing.Size(75, 23);
            this.sel_dev_ok.TabIndex = 4;
            this.sel_dev_ok.Text = "ok";
            this.sel_dev_ok.UseVisualStyleBackColor = true;
            this.sel_dev_ok.Click += new System.EventHandler(this.sel_dev_ok_Click);
            // 
            // sel_dev_cancel
            // 
            this.sel_dev_cancel.Location = new System.Drawing.Point(157, 176);
            this.sel_dev_cancel.Name = "sel_dev_cancel";
            this.sel_dev_cancel.Size = new System.Drawing.Size(75, 23);
            this.sel_dev_cancel.TabIndex = 5;
            this.sel_dev_cancel.Text = "cancel";
            this.sel_dev_cancel.UseVisualStyleBackColor = true;
            this.sel_dev_cancel.Click += new System.EventHandler(this.sel_dev_cancel_Click);
            // 
            // rb_sel_dev
            // 
            this.rb_sel_dev.AutoSize = true;
            this.rb_sel_dev.Location = new System.Drawing.Point(12, 38);
            this.rb_sel_dev.Name = "rb_sel_dev";
            this.rb_sel_dev.Size = new System.Drawing.Size(82, 17);
            this.rb_sel_dev.TabIndex = 6;
            this.rb_sel_dev.TabStop = true;
            this.rb_sel_dev.Text = "SBUSCAN2";
            this.rb_sel_dev.UseVisualStyleBackColor = true;
            this.rb_sel_dev.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rb3_sel_dev
            // 
            this.rb3_sel_dev.AutoSize = true;
            this.rb3_sel_dev.Location = new System.Drawing.Point(12, 67);
            this.rb3_sel_dev.Name = "rb3_sel_dev";
            this.rb3_sel_dev.Size = new System.Drawing.Size(82, 17);
            this.rb3_sel_dev.TabIndex = 7;
            this.rb3_sel_dev.TabStop = true;
            this.rb3_sel_dev.Text = "SBUSCAN3";
            this.rb3_sel_dev.UseVisualStyleBackColor = true;
            this.rb3_sel_dev.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // rb4_sel_dev
            // 
            this.rb4_sel_dev.AutoSize = true;
            this.rb4_sel_dev.Location = new System.Drawing.Point(11, 99);
            this.rb4_sel_dev.Name = "rb4_sel_dev";
            this.rb4_sel_dev.Size = new System.Drawing.Size(82, 17);
            this.rb4_sel_dev.TabIndex = 8;
            this.rb4_sel_dev.TabStop = true;
            this.rb4_sel_dev.Text = "SBUSCAN4";
            this.rb4_sel_dev.UseVisualStyleBackColor = true;
            this.rb4_sel_dev.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // select_device_dialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.rb4_sel_dev);
            this.Controls.Add(this.rb3_sel_dev);
            this.Controls.Add(this.rb_sel_dev);
            this.Controls.Add(this.sel_dev_cancel);
            this.Controls.Add(this.sel_dev_ok);
            this.Controls.Add(this.sel_dev_rb1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "select_device_dialog";
            this.Text = "Select Device";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton sel_dev_rb1;
        private System.Windows.Forms.Button sel_dev_ok;
        private System.Windows.Forms.Button sel_dev_cancel;
        private System.Windows.Forms.RadioButton rb_sel_dev;
        private System.Windows.Forms.RadioButton rb3_sel_dev;
        private System.Windows.Forms.RadioButton rb4_sel_dev;
    }
}